import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import Vue from "vue";
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  Vue,
  dsn: "https://ebe6f897efef43d68b2d757cf2c05ee7@o488099.ingest.sentry.io/5547830",
  integrations: [
    new Integrations.BrowserTracing(),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});
new Vue({
  render: h => h(App),
}).$mount('#app')
